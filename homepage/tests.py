from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import index
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options

class Story8Test(TestCase):

    def test_Story8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

   

    def test_Story8_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_Story8_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')